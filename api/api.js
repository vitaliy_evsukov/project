const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const UserSchema = require('./models/user');
mongoose.model('User', UserSchema);

const CompanySchema = require('./models/company');
mongoose.model('Company', CompanySchema);

const AirplaneSchema = require('./models/airplane');
mongoose.model('Airplane', AirplaneSchema);

const BreakingSchema = require('./models/breaking');
mongoose.model('Breaking', BreakingSchema);

const FimSchema = require('./models/fim');
mongoose.model('FIM', FimSchema);

const IndexfimShema = require('./models/indexfim');
mongoose.model('Indexfim', IndexfimShema);

const firewall = require('./middleware/firewall').firewall;

const auth = require('./routers/auth');
const users = require('./routers/user');
const company = require('./routers/company');

const seed = require('./routers/seed');
router.use(seed);

router.use(auth);
router.use(firewall);
router.use(users);
router.use(company);

module.exports = router;