const mongoose = require('mongoose');
const User = mongoose.model('User');
const Company = mongoose.model('Company');

const jwt = require('jsonwebtoken');
const jwtconf = require('../resources/jwt');

const BadRequestError = require('../resources/errors').BadRequestError;
const NotFoundError = require('../resources/errors').NotFoundError;
const UnauthorizedError = require('../resources/errors').UnauthorizedError;

const strings = require('../resources/strings').auth;

exports.auth = (req, res, next) => {
    if(!req.body.email || !req.body.password){return next(new BadRequestError(strings.authBadRequest));}
    User.findOne({email: req.body.email}, (err, user) => {
        if(err){return next(err);}
        if(!user || user.roles.deactivated){return next(new NotFoundError(strings.userNotFound));}
        user.comparePassword(req.body.password, (err, result) => {
            if(err){return next(err);}
            if(!result){return next(new UnauthorizedError(strings.wrongPassword));}
            let expiration = user.roles.administrator ?
                jwtconf.expiration.administrator :
                jwtconf.expiration.user;
            let token = jwt.sign({id: user.id}, jwtconf.key, {
                expiresIn: expiration
            });
            res.status(200).json({token: token});
        });
    });
}