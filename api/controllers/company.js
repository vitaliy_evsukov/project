const mongoose = require('mongoose');
const Company = mongoose.model('Company');
const User = mongoose.model('User');

const NotFoundError = require('../resources/errors').NotFoundError;
const ForbiddenError = require('../resources/errors').ForbiddenError;

const strings = require('../resources/strings').company;

exports.new = (req, res, next) => {
    if(!req.user.roles.administrator){return next(new ForbiddenError(strings.createForbidden));}
    let company = new Company(req.body);
    company.save((err, company) => {
        if(err){return next(err);}
        res.status(200).json(company);
    });
}