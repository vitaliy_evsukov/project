const mongoose = require('mongoose');
const User = mongoose.model('User');
const Company = mongoose.model('Company');

module.exports = (req, res, next) => {
    let admin = new User({
        email: 'wagnorog@gmail.com',
        password: 'dev',
        firstname: 'Виталий',
        lastname: 'Евсюков',
        roles: {
            administrator: true,
            deactivated: false
        }
    });
    admin.save({ validateBeforeSave: false }, (err, user) => {
        if(err){return next(err);}
        res.status(200).json(user);
    });
}