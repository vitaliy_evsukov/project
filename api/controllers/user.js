const mongoose = require('mongoose');
const User = mongoose.model('User');
const Company = mongoose.model('Company');

const NotFoundError = require('../resources/errors').NotFoundError;
const ForbiddenError = require('../resources/errors').ForbiddenError;

const strings = require('../resources/strings').user;

exports.all = (req, res, next) => {
    if(!req.user.roles.administrator){req.query['__roles.deactivated'] = false;}
    User.find(req.query).select('-password -roles').exec((err, users) => {
        if(err){return next(err);}
        if(users.length === 0){return next(new NotFoundError(strings.usersNotFound));}
        res.status(200).json(users);
    });
}

exports.new = (req, res, next) => {
    if(!req.user.roles.administrator){return next(new ForbiddenError(strings.createForbidden));}
    let user = new User(req.body);
    user.__createdBy = req.user.id;
    user.save((err, user) => {
        if(err){return next(err);}
        res.status(200).json(user);
    });
}

exports.one = (req, res, next) => {
    if(req.params.id === 'me'){req.params.id = req.user.id;}
    User.findById(req.params.id).select('-password -roles').exec((err, user) => {
        if(err){return next(err);}
        if(!user || user.roles.deactivated){return next(new NotFoundError(strings.userNotFound));}
        res.status(200).json(user);
    });
}

exports.edit = (req, res, next) => {
    if(!req.user.roles.administrator){return next(new ForbiddenError(strings.editForbidden));}
    User.findById(req.params.id, (err, user) => {
        if(err){return next(err);}
        if(!user || user.roles.deactivated){return next(new NotFoundError(strings.userNotFound));}
        user.set(req.body);
        user.save((err, user) => {
            if(err){return next(err);}
            res.status(200).json(user);
        });
    });
}

exports.password = (req, res, next) => {
    User.findById(req.user.id).select('-password -roles').exec((err, user) => {
        if(err){return next(err);}
        if(!user || user.roles.deactivated){return next(new NotFoundError(strings.userNotFound));}
        user.password = req.body.password;
        user.save((err, user) => {
            if(err){return next(err);}
            res.status(200).json(user);
        });
    });
}

exports.deactivate = (req, res, next) => {
    if(req.user.roles.deactivated){return next(new ForbiddenError(strings.deleteForbidden));}
    User.findById(req.params.id).select('-password -roles').exec((err, user) => {
        if(err){return next(err);}
        if(!user || user.roles.deactivated){return next(new NotFoundError(strings.userNotFound));}
        user.roles.deactivated = true;
        user.save((err, user) => {
            if(err){return next(err);}
            res.status(200).json(user);
        });
    });
}