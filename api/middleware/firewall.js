const mongoose = require('mongoose');
const User = mongoose.model('User');
const Company = mongoose.model('Company');

const jwt = require('jsonwebtoken');
const jwtconf = require('../resources/jwt');

const UnauthorizedError = require('../resources/errors').UnauthorizedError;

const strings = require('../resources/strings').firewall;

exports.firewall = (req, res, next) => {
    let token = req.headers['x-access-token'];
    if(!token){return next(new UnauthorizedError(strings.needAuth));}
    jwt.verify(token, jwtconf.key, (err, decoded) => {
        if(err){return next(err);}
        User.findById(decoded.id, (err, user) => {
            if(err){return next(err);}
            if(!user || user.roles.deactivated){return next(new UnauthorizedError(strings.needAuth));}
            req.user = user;
            next();
        });
    })
}