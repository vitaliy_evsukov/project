const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;

AirplaneSchema = new Schema ({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    companyID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
     

})

module.exports = AirplaneSchema;