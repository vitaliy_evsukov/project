const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;

BreakingSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    airplaneId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Airplane',
        required: true
    } ,
    datecreate: {
        type: Date, 
        default: Date.now
    },
    worked: {
        type: Boolean,
        default: false
    },
    indexfim: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Indexfim',
        required: true
        
    }
})

module.exports = BreakingSchema;