const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;

CompanySchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    companyType: {
        owner: {
            type: Boolean,
            required: true,
            default: false
        },
        brigade: {
            type: Boolean,
            required: true,
            default: false
        }
    }
})

module.exports = CompanySchema;