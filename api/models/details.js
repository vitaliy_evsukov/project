const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;

DetailsSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    warehouseID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Warehouse',
        required: true
    },
    cost: {
        type: Number,
        required: true
    }

})

module.exports = DetailsSchema;