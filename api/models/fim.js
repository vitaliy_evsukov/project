const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;


FimSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    adress:{
        type: String,
        required: true
    },

})

module.exports = FimSchema;