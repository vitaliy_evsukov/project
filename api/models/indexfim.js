const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;


IndexfimSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    fimID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FIM',
        required: true
    },
    statistic: {
        type: Number,
        default: 0
    },
    complexity: {
        type: Number,
        required: true
    },
    
     

})

module.exports = IndexfimSchema;