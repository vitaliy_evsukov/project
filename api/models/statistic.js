const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;


StatisticSchema = new Schema ({
    CompanyID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    statistic: {
        type: Number,
        default: 0
    },
     

})

module.exports = StatisticSchema;