const mongoose = require('mongoose');
const bcrypt = require('bcrypt-node');
const ForbiddenError = require('../resources/errors').ForbiddenError;
const Schema = mongoose.Schema;

UserSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    companyID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    roles: {
        administrator: {
            type: Boolean,
            required: true,
            default: false
        },
        deactivated: {
            type: Boolean,
            required: true,
            default: false
        }
    },
    __createdBy: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
},{
    collection: 'users',
    timestamps: {
        createdAt: '__createdAt',
        updatedAt: '__updatedAt'
    }
});

UserSchema.pre('validate', function(next){
    if(!this.isNew){
        if(this.isModified('__createdBy')){
            this.invalidate('__createdBy');
        }
    }
    next();
});

UserSchema.pre('save', function(next){
    var user = this;
    if(this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err, salt){
            if(err){return next(err);}
            bcrypt.hash(user.password, salt, null, function(err, hash){
                if(err){return next(err);}
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function(password, done) {
    bcrypt.compare(password, this.password, function(err, result){
        if(err){return done(err, null);}
        return done(null, result);
    });
}

module.exports = UserSchema;