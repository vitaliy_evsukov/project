exports.BadRequestError = class BadRequestError extends Error{
    constructor(...args){
        super(...args);
        Error.captureStackTrace(this, BadRequestError);
    }
}

exports.NotFoundError = class NotFoundError extends Error{
    constructor(...args){
        super(...args);
        Error.captureStackTrace(this, NotFoundError);
    }
}

exports.UnauthorizedError = class UnauthorizedError extends Error{
    constructor(...args){
        super(...args);
        Error.captureStackTrace(this, UnauthorizedError);
    }
}

exports.ForbiddenError = class ForbiddenError extends Error{
    constructor(...args){
        super(...args);
        Error.captureStackTrace(this, ForbiddenError);
    }
}