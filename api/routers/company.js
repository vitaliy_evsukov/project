const express = require('express');
const router = express.Router();

const company = require('../controllers/company');

router.route('/company')
    .post(company.new);

module.exports = router;