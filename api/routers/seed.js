const express = require('express');
const router = express.Router();

const seed = require('../controllers/seed');

router.get('/seed', seed);

module.exports = router;