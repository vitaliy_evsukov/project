const express = require('express');
const router = express.Router();

const user = require('../controllers/user');

router.route('/users')
    .get(user.all)
    .post(user.new);

router.route('/user/:id')
    .get(user.one)
    .put(user.edit)
    .delete(user.deactivate);

router.route('/user/me/password')
    .put(user.password);

module.exports = router;