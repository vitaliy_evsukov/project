const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const api = require('./api/api');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/project');

const app = express();
app.use(bodyparser.urlencoded({extended: true}));
app.use('/api', api);

app.listen(80, () => {
    console.log("Сервер запущен");
});